import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NavbarComponent } from './public/navigation/navbar/navbar.component';
import { HomeComponent } from './public/home/home.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AppRoutingModule } from './app-routing.module';

import { AuthService } from './auth/services/auth.service';
import { CommonService } from './services/common.service';

import { MatGridListModule, MatCardModule, MatMenuModule,
  MatIconModule, MatButtonModule, MatToolbarModule,MatCheckboxModule,
  MatSidenavModule, MatListModule, MatTableModule,
  MatPaginatorModule, MatSortModule, MatDialogModule, MatInputModule,
  MatFormFieldModule, MatOptionModule, MatSelectModule } from '@angular/material';
import { PagenotfoundComponent } from './public/pagenotfound/pagenotfound.component';
import { PlanningComponent } from './planning/planning.component';
import { SidebarComponent } from './public/navigation/sidebar/sidebar.component';
import { LayoutModule } from '@angular/cdk/layout';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent,
    PagenotfoundComponent,
    PlanningComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatGridListModule, MatCardModule, MatMenuModule,
  MatIconModule, MatButtonModule, MatToolbarModule,MatCheckboxModule,
  MatSidenavModule, MatListModule, MatTableModule,
  MatPaginatorModule, MatSortModule, MatDialogModule, MatInputModule,
  MatFormFieldModule, MatOptionModule, MatSelectModule, LayoutModule
  ],
  providers: [CommonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
