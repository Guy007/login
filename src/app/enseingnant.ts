export interface Enseignant{
        nom?: String,
        prenom?:String,
        adresse?:String,
        grade?:String,
        domaine?:String,
        contacts?:{
            email?:String,
            telephone:String
        }
}