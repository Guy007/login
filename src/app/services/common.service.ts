import { Injectable } from '@angular/core';
import { catchError, tap, map } from 'rxjs/operators';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Enseignant } from '../enseingnant';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  mode = 'Save';
  constructor(private http: HttpClient) { }


  private extractData(res: Response) {
  let body = res;
  return body || { };
}

  GetUser(){       
    return this.http.get('http://localhost:4201/api/getUser/')
    .pipe(
      map(this.extractData)
    )
  } 

  GetEns(){       
    return this.http.get('http://localhost:4201/api/getEns/')
    .pipe(
      map(this.extractData)
    )
  }
  
  SaveEns(prof: Enseignant){
    return this.http.post<Enseignant>('http://localhost:4201/api/SaveEns', prof, httpOptions)
  }
}
