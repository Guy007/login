import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EnseignantService {

  constructor( private http: HttpClient ) { }

  getEnseignant(): Observable<any>{
    const Url = 'http://localhost:4000/';
    return this.http.get(Url);
  }
}
