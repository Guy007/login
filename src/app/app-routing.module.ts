import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './public/home/home.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthGuard } from './auth/guards/auth.guard';
import { from } from 'rxjs';
import { PageEvent } from '@angular/material';
import { PagenotfoundComponent } from './public/pagenotfound/pagenotfound.component';
import { PlanningComponent } from './planning/planning.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path:'pagenotfound' , component: PagenotfoundComponent },
  { path:'planning' , component:PlanningComponent /*,  canActivate : [AuthGuard]*/ }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule],
  providers:[AuthGuard],
  declarations: []
})
export class AppRoutingModule { }
