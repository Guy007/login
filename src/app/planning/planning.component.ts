import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../services/common.service';
import { Enseignant } from '../enseingnant';
import { User } from '../user';



@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.css']
})
export class PlanningComponent implements OnInit {

  ens = {nom:'konan',prenom:'john',adresse:'abobo',grade:'Ingenieur', domaine:'developpement web',contacts: {email:'hervefab007@gmail.com', telephone:'+22507088892'} };
  public prof:Enseignant;
  public user: User ;
  public ensgn: any;
  constructor(private http: HttpClient, private Data: CommonService) {
      
    this.Data.GetUser().subscribe(res =>{
        this.user = res;
        console.log(this.user);
      });

        this.Data.GetEns().subscribe(res =>{
        this.ensgn = res;
        console.log(this.ensgn);
      });
  
     

   }

   

  ngOnInit() {
  
  this.Data.SaveEns(this.ens)  
  .subscribe(data =>  {    
     console.log(this.ens) 
  }   
 )

  }

  

  

}
