import { Component, OnInit } from '@angular/core';


export interface Tile {
  categorie: string;
}


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
 tiles: Tile[] = [];
 
  constructor() { }

  ngOnInit() {

this.tiles= [
    
    {categorie: 'Etudiants'},
    {categorie: "Seances"},
    {categorie: "Ues"},
    {categorie: "filieres"},
    {categorie: "Enseignants"}
 
  ];
  }


}
