var express = require('express');  
var path = require("path");   
var bodyParser = require('body-parser');  
var mongo = require("mongoose");  
  
var db = mongo.connect("mongodb://localhost:27017/eplanning", function(err, response){  
   if(err){ console.log( err); }  
   else{ console.log('Connected to ' + db, ' + ', response); }  
});  
  
   
var app = express()  
app.use(bodyParser());  
app.use(bodyParser.json({limit:'5mb'}));   
app.use(bodyParser.urlencoded({extended:true}));  
   
  
app.use(function (req, res, next) {        
     res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');    
     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');    
     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');      
     res.setHeader('Access-Control-Allow-Credentials', true);       
     next();  
 });  

//user 
var Schema = mongo.Schema;  
var UsersSchema = new Schema({      
 name: { type: String   },       
 address: { type: String   },   
},{ versionKey: false });  
   
  
var model = mongo.model('user', UsersSchema, 'user');  

//enseignant
var EnseignantsSchema = new Schema({
//  id:{type:Number},
  nom:{type:String},
  prenom:{type:String},
  adresse:{type:String},
  grade:{type:String},
  domaine:{type:String},
  contacts:{
    email:{type:String},
    telephone:{type:String}
  } 
},{versionKey: false});
var modelEns = mongo.model('Enseignants',EnseignantsSchema,'Enseignants');

app.post("/api/SaveUser",function(req,res){
  req.body.mode = 'Save';  
 var mod = new model(req.body);  
 if(req.body.mode =="Save")  
 {  
   console.log('dans l fonction save '+ data)
    mod.save(function(err,data){  
      if(err){  
         res.send(err);                
      }  
      else{        
          res.send({data:"Record has been Inserted..!!"});  
      }  
 });  
}  
else   
{  
 model.findByIdAndUpdate(req.body.id, { name: req.body.name, address: req.body.address},  
   function(err,data) {  
   if (err) {  
   res.send(err);         
   }  
   else{        
          res.send({data:"Record has been Updated..!!"});  
     }  
 });  
  
  
}  
 })  
  
 app.post("/api/deleteUser",function(req,res){      
    model.remove({ _id: req.body.id }, function(err) {    
     if(err){    
         res.send(err);    
     }    
     else{      
            res.send({data:"Record has been Deleted..!!"});               
        }    
 });    
   })  
  
  
  
 app.get("/api/getUser",function(req,res){  
    model.find({},function(err,data){  
              if(err){  
                  res.send(err);  
              }  
              else{                
                  res.send(data);  
                  }  
          });  
  })
 

app.post("/api/SaveEns",function(req,res){   
 var modEns = new modelEns(req.body);  
 if(modEns != 'null')  
 {  
   console.log(modEns +'dans la fonction save') ;
    modEns.save(function(err,data){ 
      
      if(err){  
         res.send(err);                
      }  
      else{        
          res.send({data:"Record has been Inserted..!!"});  
      }  
 });  
}  
else   
{  
 modelEns.findByIdAndUpdate(req.body.id, { nom: req.body.nom, prenom: req.body.prenom, adress:req.body.adrese, garde:req.body.grade},  
   function(err,data) {  
   if (err) {  
   res.send(err);         
   }  
   else{        
          res.send({data:"Record has been Updated..!!"});  
     }  
 });  
  
  
}  
 })  
  
 app.post("/api/deleteEns",function(req,res){      
    modelEns.remove({ _id: req.body.id }, function(err) {    
     if(err){    
         res.send(err);    
     }    
     else{      
            res.send({data:"Record has been Deleted..!!"});               
        }    
 });    
   })  
  
  
  
 app.get("/api/getEns",function(req,res){  
    modelEns.find({},function(err,data){  
              if(err){  
                  res.send(err);  
              }  
              else{                
                  res.send(data);  
                  }  
          });  
  })

  
  
app.listen(4201, function () {  
    
 console.log('Example app listening on port 4201!')  
}) 